"""
Análise de correlação dos pedidos cancelados por tipo de ocorrência
Criado em: 11/08/2021
Criado por: Felipe Flumignan
"""

""" Importando bibliotecas """
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from yellowbrick.regressor import ResidualsPlot

""" Instanciando o arquivo de dados CSV """
dados = pd.read_csv('./dados.csv')

""" Setando os valores de x e y """
y = dados.iloc[:, 0].values
x = dados.iloc[:, 1].values

""" Analisando a correlação para validarmos o conceito """
# correl = np.corrcoef(x, y)
# print(correl)

""" Remodelando a matriz do eixo x """
x = x.reshape(-1, 1)

""" Ajustando o modelo de dados para a predição """
regres = LinearRegression()
regres.fit(x, y)

""" Analisando a predição do constante + coeficiente """
# prev = regres.intercept_ + regres.coef_
# print(prev)

""" Analisando o score (R quadrado da correlação) """
# score = regres.score(x, y)
# print(score)

""" Configurando o gráfico residual """
visual = ResidualsPlot(regres)
""" Ajustando o modelo residual """
visual.fit(x, y)

""" Plotando os valores do dataframe (histórico) """
plt.scatter(x, y)

""" Plotando a predição com base no histórico """
plt.plot(x, regres.predict(x), color='red')

""" Configurando as labels dos eixos do gráfico """
plt.ylabel('Produtos por Pedido')
plt.xlabel('Ocorrências por Pedido')

""" Configurando o título do gráfico """
plt.title('Correlação dos Pedidos Cancelados por Ocorrências')

""" Salva o gráfico na pasta do projeto """
# plt.savefig('/export/dispersao.png', dpi=600, transparente=True)

""" Exibe o gráfico de correlação """
plt.show()

""" Exibe o gráfico residual da correlção e o score (R quadrado da correlação) """
visual.show()
